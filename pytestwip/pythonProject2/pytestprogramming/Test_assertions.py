import pytest

#assert
def test_simple_assertion():
    assert 1+1 == 2

#assert x==y
def test_equal_assertion():
    x = 5
    y = 5
    assert x==y

#assert x!=y
def test_not_equal_assertion():
    x = 5
    y = 10
    assert x!=y

#assert x in y
def test_in_assertion():
    numbers=[1,2,3,4,5]
    assert 3 in numbers

#assert x not in y
def test_not_in_assertion():
    fruits = ["apple","banana","cherry"]
    assert "orange" not in fruits

#assert x is y

def test_identity_assertion():
    a = [1,2,3]
    b = a
    assert a is b

