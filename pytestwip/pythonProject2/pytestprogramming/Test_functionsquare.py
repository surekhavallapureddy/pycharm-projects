import pytest

def square(n):
    return n**2

@pytest.mark.parametrize("number, expected_result", [(2, 4), (3, 9), (4, 16)])
def test_square(number, expected_result):
    assert square(number) == expected_result