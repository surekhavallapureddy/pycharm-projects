import pytest

# Function to remove the letter 'g' from a sentence
def remove_g(sentence):
    return sentence.replace('g', '').replace('G', '')


@pytest.mark.parametrize("input_sentence, expected_output", [
    (" grapes grow in the garden.", " rapes row in the arden."),
    ("Good game!", "ood ame!"),
    ("Great going!", "reat oin!")
])

def test_remove_g(input_sentence, expected_output):
    assert remove_g(input_sentence) == expected_output
