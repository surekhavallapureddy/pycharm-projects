import pytest
@pytest.fixture(scope = 'function')
def dbconnection():
    print("open the db connection")
    yield
    print("close the db connection")
@pytest.mark.usefixture("dbconnection")
def test_createquery():
    print("table is created")
@pytest.mark.usefixtures("dbconnection")
def test_selectquery():
    print("table is selected")
@pytest.mark.usefixtures("dbconnection")
def test_deletequery():
    print("table is deleted")