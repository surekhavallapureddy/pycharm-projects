#area module
def circle(radius):
    return 3.14 * radius ** 2

def square(side):
    return side ** 2

def rectangle(length, breadth):
    return length * breadth

def paralleogram(base, height):
    return base * height

def triangle(base, height):
    return 1/2 * (base * height)


# factorial
def factorial(n):
    if n < 0:
        raise ValueError("Factorial is not defined for negative numbers")
    if n == 0:
        return 1
    result = 1
    for i in range(1, n + 1):
        result *= i
    return result

