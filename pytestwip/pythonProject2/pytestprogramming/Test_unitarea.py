import area

class Testarea:

    def test_circle(self):
        assert area.circle(14) == 615.44
        assert area.circle(-14) == 615.44
        #assert area.circle(-14) == 610 #failed

    def test_square(self):
        assert area.square(4) == 16
        assert area.square(-4) == 16
        #assert area.square(-4) == 13 #faile

    def test_rectangle(self):
        assert area.rectangle(2,4) == 8 #passed
        #assert area.rectangle(2, 4) == 9 #failed

    def test_paralleogram(self):
        assert area.paralleogram(6,7) == 42 #passed
        #assert area.paralleogram(6, 7) == 44 #failed


    def test_triangle(self):
        assert area.triangle(6,4) == 12 #passed
        #assert area.triangle(6,4) == 41 #failed

 def test_factorial(self):
        assert area.factorial(0) == 1
        assert area.factorial(1) == 1
        assert area.factorial(5) == 120
        assert area.factorial(10) == 3628800