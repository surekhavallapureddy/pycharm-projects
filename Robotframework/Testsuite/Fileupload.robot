*** Settings ***
Library  SeleniumLibrary
Library  OperatingSystem

*** Variables ***
${browser}   chrome
${url}    https://the-internet.herokuapp.com/upload
${file_path}    C:\\Users\\vjais\\OneDrive\\Pictures\\Screenshots\\giri second year marks.png

*** Test Cases ***
Verify File Upload
    Open Browser    ${url}    ${browser}
    Maximize Browser Window
    Sleep    4s
    # Wait for the file input element to be visible
    Wait Until Element Is Visible    xpath://input[@id='file-upload']    timeout=10
    # Use Choose File to upload the file
    Choose File    xpath://input[@id='file-upload']    ${file_path}
    Sleep    4s
    # Click the upload button to submit the form
    Click Button    xpath://input[@id='file-submit']
    Sleep    4s
    # Verify the upload was successful
    Wait Until Element Is Visible    xpath://div[@id='uploaded-files']    timeout=10
    Element Should Contain    xpath://div[@id='uploaded-files']    giri second year marks.png
    Close Browser