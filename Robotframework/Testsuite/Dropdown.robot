*** Settings ***
Library  SeleniumLibrary

*** Variables ***

${browser}      Chrome
${url}      https://the-internet.herokuapp.com/dropdown

*** Test Cases ***
RadioButton
        Open Browser        ${url}    ${browser}
        maximize browser window
        Sleep   4s
        Wait Until Element Is Visible        id:dropdown     timeout=8
        Sleep   4s
        Select From List By Index       id:dropdown     1
        Sleep   4s
        Select From List By Value       id:dropdown     2
        Sleep   4s
        List Selection Should Be    id:dropdown     Option 2
        Sleep   2s
        Close Browser