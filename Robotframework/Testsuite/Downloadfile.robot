*** Settings ***
Documentation  File Upload Download in Robot Framework
Library  SeleniumLibrary
Library   OperatingSystem

*** Variables ***

*** Test Cases ***
Verify File Download
    [documentation]  This test case verifies that a user can successfully upload a file
    [tags]  Regression
    Open Browser  https://the-internet.herokuapp.com/download  Chrome
    Click Element  xpath://a[normalize-space()='giri second year marks.png']
    Sleep  3s
    ${files}    List Files In Directory     C:/Users/vjais/Documents/wipro
    Length Should Be    ${files}    3
    Close Browser