*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${browser}  chrome
${url}  https://chercher.tech/practice/practice-pop-ups-selenium-webdriver

*** Test Cases ***
HandleAlerts
    [Documentation]  Test case for handling simple, confirmational, and prompt alerts
    open browser        ${url}    ${browser}
    maximize browser window
    Sleep   4s

    # Handling simple alert
    Click Element       xpath://input[@name='alert']
    Sleep   2s
    ${alertmessage} =  Handle Alert  action=ACCEPT  timeout=3
    Log To Console  Simple Alert Message: ${alertmessage}

    # Handling confirmational alert
    Click Element       xpath://input[@name='confirmation']
    Sleep   2s
    ${alertmessage} =  Handle Alert  action=DISMISS  timeout=3
    Log To Console  Confirmational Alert Message: ${alertmessage}

    # Handling prompt alert
    Click Element       xpath://input[@name='prompt']
    Sleep   2s
    Input Text Into Alert  Harsha  timeout=3
    ${alertmessage} =  Handle Alert  action=ACCEPT  timeout=3
    Log To Console  Prompt Alert Message: ${alertmessage}

    Close Browser
