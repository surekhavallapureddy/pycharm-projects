*** Settings ***
Library  SeleniumLibrary


*** Variables ***

*** Test Cases ***

Verify screenshot
     Open Browser       https://jqueryui.com/datepicker/    chrome
     Maximize Browser Window
     Sleep    5s
     Select Frame    xpath://iframe[@class='demo-frame']
     Sleep    5s
     Click Element    xpath://input[@id='datepicker']
     Sleep    5s
     Click Element    xpath://a[contains(text(),'28')]
     Sleep    5s
     Close Browser
