import http.client
import logging

# Configure the logging
logging.basicConfig(filename="httpclienterrors.log", level=logging.ERROR)

try:
    conn = http.client.HTTPSConnection("videogamedb.uk", 443)
    conn.request("GET", "/api/videogae/")
    response = conn.getresponse()
    # Check the status code
    if response.status == 200:
        print("Request successful")
    elif response.status == 404:
        print("Resource not found")
    else:
        print("Unexpected status code:", response.status)
except http.client.HTTPException as e:
    logging.error("HTTP exception: %s", e)
except Exception as e:
    logging.error("An error occurred: %s", e)
finally:
    conn.close()