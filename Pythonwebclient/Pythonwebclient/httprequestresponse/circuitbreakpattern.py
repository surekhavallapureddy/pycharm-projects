import http.client
import circuitbreaker


@circuitbreaker.circuit
def make_request():
    conn = http.client.HTTPSConnection("videogamedb.uk", 443)
    conn.request("GET", "/api/videogame/")
    response = conn.getresponse()
    conn.close()

    # Check the status code
    if response.status == 200:
        return "Request successful"
    else:
        raise Exception("Request failed with status code:", response.status)


try:
    result = make_request()
    print(result)
except circuitbreaker.CircuitBreakerError as e:
    print("Circuit breaker is open, request not sent")
except Exception as e:
    print("An error occurred:", e)