import requests

# Send a GET request
r = requests.get("https://videogamedb.vk:443/api/v2/videogame/2")

# Fetch the JSON format of the response
print("JSON Response:", r.json())

# Text of the response
print("Text Response:", r.text)

# Status code of the response
print("Status Code:", r.status_code)

# Response URL
print("Response URL:", r.url)

# Request object
print("Request Object:", r.request)

# Check the encoding of the response
print("Response Encoding:", r.encoding)

# Fetch the headers
print("Response Headers:", r.headers)

# Response history
print("Response History:", r.history)

# Close the connection
r.close()