import http.client
try:
    conn = http.client.HTTPSConnection("https://videogamedb.uk:443")
    conn.request("GET", "/api/videogae/")
    response = conn.getresponse()
    # check the status code
    if response.status == 200:
        print("request successful")
    elif response.status == 404:
        print("Resource not found")
    else:
        print("unexpected status code:", response.status)
except http.client.HTTPException as e:
    print("Http exception",e)
except Exception as e:
    print("An error occured",e)
finally:
    conn.close()