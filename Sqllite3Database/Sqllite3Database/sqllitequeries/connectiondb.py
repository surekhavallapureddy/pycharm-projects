import sqlite3

# create a connection to the data base
connection = sqlite3.connect('employee.db')
# cursor object
cursor_obj = connection.cursor()
#creating table
table = """ CREATE TABLE employee (
            Employeename VARCHAR(255) NOT NULL,
            EmployeeID INT,
            EmployeeBU VARCHAR(255) NOT NULL,
            EmployeeSalary INT
);"""
cursor_obj.execute(table)

#insert data to the employee table
cursor_obj.execute("""INSERT INTO employee(Employeename,EmployeeID,EmployeeBU,EmployeeSalary) VALUES("John",67788,"HR",767878)""")
cursor_obj.execute("""INSERT INTO employee(Employeename,EmployeeID,EmployeeBU,EmployeeSalary) VALUES("Tina",45558,"IT",344556)""")
cursor_obj.execute("""INSERT INTO employee(Employeename,EmployeeID,EmployeeBU,EmployeeSalary) VALUES("Peter",466634,"SUP",345556)""")
cursor_obj.execute("""INSERT INTO employee(Employeename,EmployeeID,EmployeeBU,EmployeeSalary) VALUES("Jaya",323444,"HR",323444)""")
cursor_obj.execute("""INSERT INTO employee(Employeename,EmployeeID,EmployeeBU,EmployeeSalary) VALUES("Ravi",344545,"IT",233446)""")
cursor_obj.execute("""INSERT INTO employee(Employeename,EmployeeID,EmployeeBU,EmployeeSalary) VALUES("Reema",33445,"SUP",545656)""")
cursor = cursor_obj.execute("select * from employee")

for row in cursor:
    print(row)

connection.commit()
connection.close()
