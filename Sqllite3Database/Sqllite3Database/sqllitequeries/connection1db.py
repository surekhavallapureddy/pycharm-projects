import sqlite3
# create a connection to the database
connection = sqlite3.connect('StudentRepo')
# cursor object
cursor_obj = connection.cursor()

table = """ CREATE TABlE  StudentRepo(
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL,
    class TEXT NOT NULL,
    mark INTEGER NOT NULL,
    gender TEXT NOT NULL
);"""
cursor_obj.execute(table)

# insert data to the StudentRepo table
cursor_obj.execute("""INSERT INTO StudentRepo (id,name,class,mark,gender)VALUES(1,"John Deo","Four",75,"Female")""")
cursor_obj.execute("""INSERT INTO StudentRepo (id,name,class,mark,gender)VALUES(2,"Max Ruin","Three",85,"male")""")
cursor_obj.execute("""INSERT INTO StudentRepo (id,name,class,mark,gender)VALUES(3,"Amold","Three",55,"male")""")
cursor_obj.execute("""INSERT INTO StudentRepo (id,name,class,mark,gender)VALUES(4,"Krish Star","Four",60,"Female")""")
cursor_obj.execute("""INSERT INTO StudentRepo (id,name,class,mark,gender)VALUES(5,"John Mike","Four",60,"Female")""")
cursor_obj.execute("""INSERT INTO StudentRepo (id,name,class,mark,gender)VALUES(6,"Alex John","Four",55,"male")""")
cursor_obj.execute("""INSERT INTO StudentRepo (id,name,class,mark,gender)VALUES(7,"My John Rob","Five",78,"male")""")
cursor_obj.execute("""INSERT INTO StudentRepo (id,name,class,mark,gender)VALUES(8,"Asruid","Five",85,"male")""")
cursor_obj.execute("""INSERT INTO StudentRepo (id,name,class,mark,gender)VALUES(9,"Tes Qry","Six",78,"male")""")
cursor_obj.execute("""INSERT INTO StudentRepo (id,name,class,mark,gender)VALUES(10,"Big John","Four",55,"Female")""")

cursor = cursor_obj.execute("select * from StudentRepo")
for row in cursor:
    print(row)
connection.commit()
connection.close()

# Update the marks of Arnold (record with id = 3) to 70
cursor = cursor_obj.execute("""UPDATE StudentRepo SET mark = 70 WHERE id = 3""")
for row in cursor:
    print(row)

# Delete the row of Tes Qry (record with id = 9)
cursor = cursor_obj.execute("""DELETE FROM StudentRepo WHERE id = 9""")
for row in cursor:
    print(row)

# Query the table to sort by marks in ascending order
print("Students sorted by marks (ASC):")
cursor = cursor_obj.execute("SELECT * FROM StudentRepo ORDER BY mark ASC")
for row in cursor:
    print(row)

# Query the table to sort by marks in ascending order
print("Students sorted by marks (ASC):")
cursor = cursor_obj.execute("SELECT * FROM StudentRepo ORDER BY mark ASC")
for row in cursor:
    print(row)

# Query to select only female students
print("Female students enrolled:")
cursor = cursor_obj.execute("SELECT * FROM StudentRepo WHERE gender = 'Female'")
female_students = cursor.fetchall()
for row in female_students:
    print(row)
print(f"\nNumber of female students enrolled: {len(female_students)}")

# Query to select the student with the lowest marks
print("Student with the lowest marks:")
cursor = cursor.execute("SELECT * FROM StudentRepo ORDER BY mark ASC LIMIT 1")
lowest_marks_student = cursor.fetchone()
print(lowest_marks_student)


# Query to select the student with the highest marks
print("Student with the highest marks:")
cursor = cursor.execute("SELECT * FROM StudentRepo ORDER BY mark DESC LIMIT 1")
highest_marks_student = cursor.fetchone()
print(highest_marks_student)








