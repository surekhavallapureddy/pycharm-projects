import sqlite3
# create a connection to the database
connection = sqlite3.connect('hospital.db')
cursor_obj = connection.cursor()
#create first table
table = """CREATE TABLE department (
    departmentid INT PRIMARY KEY,
    name VARCHAR(50),
    head VARCHAR(50)
);"""
cursor_obj.execute(table)

#insert data into department table
cursor_obj.execute("""INSERT INTO department (departmentid, name, head) VALUES (1, 'General Medicine', NULL)""")
cursor_obj.execute("""INSERT INTO department (departmentid, name, head) VALUES (2, 'Surgery', NULL)""")
cursor_obj.execute("""INSERT INTO department (departmentid, name, head) VALUES (3, 'Psychiatry', NULL)""")
cursor = cursor_obj.execute("SELECT * FROM department")
for row in cursor:
    print(row)
connection.commit()
connection.close()

# create physicain table
table = """
CREATE TABLE physicain (
    employeeid INTEGER PRIMARY KEY,
    name TEXT NOT NULL,
    position TEXT NOT NULL,
    ssn TEXT NOT NULL,

);"""


#insert data into physicain table
cursor_obj.execute("""
INSERT INTO physicain (employeeid, name, position, ssn) VALUES
(1, 'John Dorian', 'Staff Internist', '111111111'),
(2, 'Elliot Reid', 'Attending Physician', '222222222'),
(3, 'Christopher Turk', 'Surgical Attending Physician', '333333333'),
(4, 'Percival Cox', 'Senior Attending Physician', '444444444'),
(5, 'Bob Kelso', 'Head Chief of Medicine', '555555555'),
(6, 'Todd Quinlan', 'Surgical Attending Physician', '666666666'),
(7, 'John Wen', 'Surgical Attending Physician', '777777777'),
(8, 'Keith Dudemeister', 'MD Resident', '888888888'),
(9, 'Molly Clock', 'Attending Psychiatrist', NULL);
""")
cursor = cursor_obj.execute("select * from physicain")
for row in cursor:
    print(row)
connection.commit()
connection.close()


SELECT p.employeeid, p.name AS physician_name, p.position, d.name AS department_name
FROM physician p
INNER JOIN department d ON p.departmentid = d.departmentid;


SELECT p.employeeid, p.name AS physician_name, p.position, d.name AS department_name
FROM physician p
RIGHT JOIN department d ON p.departmentid = d.departmentid;


SELECT p.employeeid, p.name AS physician_name, p.position, d.name AS department_name
FROM physician p
LEFT JOIN department d ON p.departmentid = d.departmentid;





