import mysql.connector

# Connect to MySQL Workbench
conn = mysql.connector.connect(
    host="localhost",
    user="root",
    password="Bhagya@12",
    database="wiprodatabase"

)
# Create a cursor object
cursor = conn.cursor()

# Create Physician table
create_physician_table_query = """
CREATE TABLE IF NOT EXISTS Physician (
    employeeid INT PRIMARY KEY,
    name VARCHAR(255),
    position VARCHAR(255),
    ssn INT
)
"""

cursor.execute(create_physician_table_query)

# Insert data into Physician table
insert_physician_data_query = """
INSERT INTO Physician (employeeid, name, position, ssn) VALUES 
    (1, 'John Dorian', 'Staff Internist', 111111111),
    (2, 'Elliot Reid', 'Attending Physician', 222222222),
    (3, 'Christopher Turk', 'Surgical Attending Physician', 333333333),
    (4, 'Percival Cox', 'Senior Attending Physician', 444444444),
    (5, 'Bob Kelso', 'Head Chief of Medicine', 5555555555),
    (6, 'Todd Quinlan', 'Surgical Attending Physician', 666666666),
    (7, 'John Wen', 'Surgical Attending Physician', 777777777),
    (8, 'Keith Dudemeister', 'MD Resident', 888888888),
    (9, 'Molly Clock', 'Attending Psychiatrist', 999999999)
"""
# Create Department table
create_department_table_query = """
CREATE TABLE IF NOT EXISTS Department (
    departmentid INT PRIMARY KEY,
    name VARCHAR(255),
    head INT
)
"""
cursor.execute(create_department_table_query)
# Insert data into Department table
insert_department_data_query = """
INSERT INTO Department (departmentid, name, head) VALUES 
    (1, 'General Medicine', 4),
    (2, 'Surgery', 7),
    (3, 'Psychiatry', 9)
"""
cursor.execute(insert_department_data_query)

# Commit changes
conn.commit()

# Perform INNER JOIN
inner_join_query = """
SELECT Physician.name AS physician_name, Department.name AS department_name
FROM Physician
INNER JOIN Department ON Physician.employeeid = Department.head
"""
cursor.execute(inner_join_query)
inner_join_result = cursor.fetchall()
print("INNER JOIN result:")
for row in inner_join_result:
    print(row)


# Perform LEFT JOIN
left_join_query = """
SELECT Physician.name AS physician_name, Department.name AS department_name
FROM Physician
LEFT JOIN Department ON Physician.employeeid = Department.head
"""
cursor.execute(left_join_query)
left_join_result = cursor.fetchall()
print("\nLEFT JOIN result:")
for row in left_join_result:
    print(row)


# Perform RIGHT JOIN (using LEFT JOIN and UNION)
right_join_query = """
SELECT Physician.name AS physician_name, Department.name AS department_name
FROM Department
LEFT JOIN Physician ON Physician.employeeid = Department.head
UNION
SELECT Physician.name AS physician_name, Department.name AS department_name
FROM Physician
LEFT JOIN Department ON Physician.employeeid = Department.head
"""
cursor.execute(right_join_query)
right_join_result = cursor.fetchall()
print("\nRIGHT JOIN result:")
for row in right_join_result:
    print(row)

# Perform FULL OUTER JOIN (using LEFT JOIN and UNION)
full_outer_join_query = """
SELECT Physician.name AS physician_name, Department.name AS department_name
FROM Physician
LEFT JOIN Department ON Physician.employeeid = Department.head
UNION
SELECT Physician.name AS physician_name, Department.name AS department_name
FROM Department
LEFT JOIN Physician ON Physician.employeeid = Department.head
"""
cursor.execute(full_outer_join_query)
full_outer_join_result = cursor.fetchall()
print("\nFULL OUTER JOIN result:")
for row in full_outer_join_result:
    print(row)

# Close the cursor and connection
cursor.close()
conn.close()