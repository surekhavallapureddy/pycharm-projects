# normal lists
numbers = [12,13,14]
doubled = [x * 2 for x in numbers]
print(doubled)

#using the list comprehension
doubled_numbers = [num * 2 for num in numbers]
print(doubled_numbers)

#normal method

div_by_7 = [num for num in range(1,1000) if num % 7 == 0]
print(div_by_7)

#find even and odd in the range of 0 to 20
even = [num for num in range(20) if num % 2 == 0]
print(even)

odd = [num for num in range(20) if num%2 != 0]
print(odd)

# matrix display
matrix = [[j for j in range(3)] for i in range (3)]
print(matrix)

# if else condition in list comprehenisons
number = [1,2,3,4,5,6]
# find even and odd numbers
even_or_odd = ["Even" if i %2 == 0 else "Odd" for i in number]
print(even_or_odd)

#if else nested condition
num_list = [y for y in range(100) if y%2 ==0 if y%5==0]
print(num_list)

# print the range >50 using integers

new_list = [x for x in range(100) if x > 50]
print(new_list)