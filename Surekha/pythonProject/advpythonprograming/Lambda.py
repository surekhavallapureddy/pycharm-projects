#normal function defintion

def add(a,b):
    result = a+b
    return result
print(add(6,7))

#Using lambda

add = lambda a,b: a+b
print(add(6,7))

#eg2
add = lambda x : x + 100
print(add(50))

#eg3
product = lambda x,y,z: x*y*z
print(product(x=5,y=6,z=9))

#pass arguments in the lambda function
addition = lambda*args : sum(args)
print(addition(20,4,6,7,7,7,7,7))