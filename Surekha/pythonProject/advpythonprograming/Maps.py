list1 = [10,40,60,80,100,200,300]
double = list(map(lambda x : x*2, list1))
print(double)


#power of 2

list1 = [2,3,4,5]
power = list(map(lambda x : x **2 ,list1))
print(power)


# divisible by 10

list1 = [10,5,6,8,11,20,35,40,20]
divby10 =list(map(lambda x : x % 10 == 0 , list1))
print(divby10)

#maps example

list(map(lambda var : var*2, range(0,10)))


#map executes all the conditions of a function on the items in the iterable objects

#filter

list(filter(lambda var : var%2 == 0,range(0,10)))


#reduce
from functools import reduce
list2 = [2,3,4,5,6,7,8,9]
sum = reduce((lambda x,y :x + y),list2)
print(sum)

