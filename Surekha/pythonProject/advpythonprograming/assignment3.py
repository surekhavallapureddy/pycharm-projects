def cube_generator(n):
    for i in range(1,n+1):
        yield i ** 3


n = 4
for cube in cube_generator(n):
    print(cube)

#prime numbers of between two numbers
def prime_num(n):
   if n < 2:
       return False
   for i in range(2, int(n ** 0.5) +1):
       if n % i == 0:
           return False
   return True
def prime_gen(start,end):
    for n in range(start,end+1):
        if prime_num(n):
            yield n
for prime in prime_gen(10,50):
    print(prime)