#revstringdecorator
def rev_string(func):
    def string():
        string_org = func()
        rev_string = string_org[::-1]
        return rev_string
    return string

@rev_string
def string_word():
    return "hello,everyone"
print(string_word())

# divide
def divide(func):
    def inner(a,b):
        return a/b
    return inner
@divide
def num(a,b):
    return a/b
print(num(a=3 ,b= 4))