#iterator with iter () to initialize the list,next method to-iterate among the object
list = [4,78,98,45]
#iterator with iter () to intialize the list
my_iter = iter(list)
print(next(my_iter))
print(next(my_iter))
print(next(my_iter))
print(next(my_iter))

#iterate among strings

iterable_value = "helloworld"
iterable_obj = iter(iterable_value)
for object in iterable_obj:
    print(object)

#iterate among intergers
list = [7,8,6,5]
for object in list:
    print(object)