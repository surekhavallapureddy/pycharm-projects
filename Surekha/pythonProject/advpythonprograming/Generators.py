# simple a generator using for loop yield ststement
def simple():
    yield 1
    yield 2
    yield 3

for value in simple():
    print(value)


#create the object of the generator
def simple_gen():
    yield 1
    yield 2
    yield 3
x = simple_gen()
print(next(x))
print(next(x))
print(next(x))