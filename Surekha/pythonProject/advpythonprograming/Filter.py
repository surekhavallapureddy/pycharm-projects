#filter the string

sub_string = lambda string : string in "welcome to python programming"
print(sub_string('python'))


#filer lists

num = [10,20,30,40,50,60,70]
greater = list(filter(lambda num : num>30, num))
print(greater)

#even and odd

odd = [7,4,1,3,6,9]
odd_filter = list(filter(lambda x: x % 2 == 1, odd))
print(odd_filter)

even = [7,4,1,3,6,9]
even_filter = list(filter(lambda x: x % 2 == 0, even))
print(even_filter)

# div by 4

num = [3,16,20,23,34]
div_by_4= list(filter(lambda x : x % 4 == 0, num))
print(div_by_4)

# age is greater than 18

age = [18, 23, 25, 7, 40, 20]

age_filter = list(filter(lambda x : x >= 18, age))
print(age_filter)
