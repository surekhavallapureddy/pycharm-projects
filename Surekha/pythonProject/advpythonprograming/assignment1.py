# remove floats numbers in list decimals
number = [1,1.2,3,5.5,21,23,4.5,1.3]
remove_float = [num for num in number if num == int(num)]
print(remove_float)

# count the no.of spaces in string
string_word = "This is a python class sample words in string"
space_count = len([spaces for spaces in string_word if spaces == ' '])
print(space_count)

#finding common numbers in two list
list_1 = [1,2,3,4,5,6]
list_2 = [2,4,6,8,9,10]
common_num = [num for num in list_1 if num in list_2]
print(common_num)

#consonants in a string
string = "hello world"
vowels = ['a', 'e', 'o' , 'u', 'i']
consonants = [char for char in string if char not in vowels]
print("".join(consonants))