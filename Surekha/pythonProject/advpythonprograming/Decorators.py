def make_pretty(func):
    # define the inner function
    def inner():
        print("I am decorated")
        func()
    return inner

# define an ordinary function
def ordinary():
    print("I am ordinary")

# call the function

dec_fun_value = make_pretty(ordinary)
dec_fun_value()