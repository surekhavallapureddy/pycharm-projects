# power of 2 using the custom iteration
class Power:
    def __init__(self,number):
        self.number = number
    def next_(self):
        #return the next iterator element
        self.number **= 2
        return self.number

p = Power(2)
print(p.next_())
print(p.next_())
print(p.next_())


# iterate the set of tuples using a range value
t = (1,2,3,4,5,6)
iterate_tuple = iter(t)
for obj in iterate_tuple:
    print(obj)